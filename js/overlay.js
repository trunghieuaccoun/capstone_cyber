var overlay = document.querySelector(".overlay");
function openOverlay() {
    overlay.classList.add("open");
}
function closeOverlay() {
    overlay.classList.remove("open");
}